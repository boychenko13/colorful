$(document).ready(function($) {
    
   sliderInit();
   accordionInit();
   initValidation();
   initMap();
   initMenu();
    initWaypoint();

    $('.button-up').on('click tap', function(){
        $('html, body').animate({
            scrollTop : 0
        },900);
    });
    $('#menu ul li a').on('click', function(){
        $(this).mPageScroll2id();
    });
});


function sliderInit(){
	$('.one-page-slider').slick({
        arrows: true,
        dots: true
    });
}

function initMenu() {
    $("#menu").mmenu({
        "counters": true,
        "offCanvas": {
            "zposition": "front"
        }
    });

    let API = $("#menu").data( "mmenu" );
    let menuWidth;
    let openFlag = false;

    $(".mobile-menu").click(function() {

        if (openFlag == false) {
            openFlag = true;
            menuWidth = $('#menu').width();
            $('.mobile-menu').addClass('is-active');
            $('.mobile-menu-js').css('left', menuWidth + 5);
            API.open();
        }
        else {
            openFlag = false;
            $('.mobile-menu').removeClass('is-active');
            $('.mobile-menu-js').css('left', '');
            API.close();
        }
    });

    API.bind('open:start', function() {
        menuWidth = $('#menu').width();
        $('.mobile-menu').addClass('is-active');
        $('.mobile-menu-js').css('left', menuWidth +5);
    });

    API.bind('close:start', function() {
        $('.mobile-menu').removeClass('is-active');
        $('.mobile-menu-js').css('left', '');
    });
}

function initValidation() {
    $('#main-form').validate({
        rules: {
            name: {
                required:true,

            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                phoneUS: true
            }
        }
    });
}

function accordionInit(){
    $('.faq-item__header').on('click', function() {
       $('.faq-item__body').stop();
        if (!$(this).closest('.faq-item').hasClass('is-active')) {
            $('.faq-item').removeClass('is-active');
            $('.faq-item__body').slideUp();
        }
        $(this).closest('.faq-item').toggleClass('is-active');
        $(this).closest('.faq-item').find('.faq-item__body').slideToggle();
        $('.faq-item__header').bind('click');
    });
}

function initWaypoint(){
    let waypointRed =  $('.mm-red').waypoint({
        handler: function (direction) {
            if (direction === 'down') {
                $('.mobile-menu').removeClass('mobile-menu--contrast');
                $('.mobile-menu').addClass('mobile-menu--contrast');
            }
        }
    })
    let waypointWhite =  $('.mm-white').waypoint({
        handler: function (direction) {
            if (direction === 'down') {
                $('.mobile-menu').removeClass('mobile-menu--contrast');
            }
        }
    })
    let waypointRedReverse =  $('.mm-red').waypoint({
        handler: function (direction) {
            if (direction === 'up') {
                $('.mobile-menu').removeClass('mobile-menu--contrast');
                $('.mobile-menu').addClass('mobile-menu--contrast');
            }
        },
        offset: '-100%'
    })
    let waypointWhiteReverse =  $('.mm-white').waypoint({
        handler: function (direction) {
            if (direction === 'up') {
                $('.mobile-menu').removeClass('mobile-menu--contrast');
            }
        },
        offset: '-100%'
    })
}

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 40.674, lng: -73.945},
        zoom: 12,
        disableDefaultUI: true,
        styles: [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#181818"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1b1b1b"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#2c2c2c"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8a8a8a"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#373737"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#3c3c3c"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4e4e4e"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#3d3d3d"
                    }
                ]
            }
        ]
    });
}